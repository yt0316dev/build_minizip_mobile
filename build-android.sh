#/bin/sh

set -e

cd $(dirname $0)
ROOT=$(perl -MCwd -le 'print Cwd::abs_path(".")')

MINIZIP="${ROOT}/libs/minizip"

WORK_DIR="${ROOT}/work/android"
mkdir -p "${WORK_DIR}"

OUT_DIR="${WORK_DIR}/out/"
mkdir -p "${OUT_DIR}"

for TARGET in Debug Release
do
  for ARCH in arm64-v8a armeabi-v7a x86_64 x86
  do
    BUILD_DIR="${WORK_DIR}/build/${TARGET}/${ARCH}"
    mkdir -p "${BUILD_DIR}"
    cd "${BUILD_DIR}"

    cmake "${MINIZIP}" \
      -DCMAKE_INSTALL_PREFIX="${OUT_DIR}/${TARGET}/${ARCH}" \
      -DCMAKE_TOOLCHAIN_FILE="${NDK}/build/cmake/android.toolchain.cmake" \
      -DANDROID_ABI="${ARCH}" \
      -DBUILD_SHARED_LIBS=YES \
      -DCMAKE_BUILD_TYPE="${TARGET}" \
      -DMZ_BZIP2=OFF \
      -DMZ_COMPAT=OFF \
      -DMZ_LZMA=OFF \
      -DMZ_SIGNING=OFF \
      -DMZ_WZAES=OFF \
      -DMZ_ZSTD=OFF

    cmake --build . --target install

    JNI_LIBS_ARCH="${ROOT}/out/android/${TARGET}/jniLibs/${ARCH}"
    mkdir -p "${JNI_LIBS_ARCH}"
    cp -f "${OUT_DIR}/${TARGET}/${ARCH}/lib/libminizip.so" "${JNI_LIBS_ARCH}"
  done
done
