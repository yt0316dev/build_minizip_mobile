#/bin/sh

set -e

cd $(dirname $0)
ROOT=$(perl -MCwd -le 'print Cwd::abs_path(".")')

MINIZIP="${ROOT}/libs/minizip"

WORK_DIR="${ROOT}/work/ios"
mkdir -p "${WORK_DIR}"

for TARGET in Debug Release
do
  for PLATFORM in simulator device
  do
    INSTALL_DIR="${WORK_DIR}/out/${PLATFORM}/${TARGET}"
    mkdir -p "${INSTALL_DIR}"

    BUILD_DIR="${WORK_DIR}/build/${PLATFORM}/${TARGET}"
    mkdir -p "${BUILD_DIR}"
    cd "${BUILD_DIR}"

    if [ ${PLATFORM} = "simulator" ]; then
      CMAKE_PLATFORM="SIMULATOR64"
      ICONV_DIR="$(xcrun --sdk iphonesimulator --show-sdk-path)/usr/include"
    else
      CMAKE_PLATFORM="OS"
      ICONV_DIR="$(xcrun --sdk iphoneos --show-sdk-path)/usr/include"
    fi

    cmake "${MINIZIP}" \
      -G Xcode \
      -DCMAKE_INSTALL_PREFIX="${INSTALL_DIR}" \
      -DCMAKE_TOOLCHAIN_FILE="${ROOT}/libs/ios-cmake/ios.toolchain.cmake" \
      -DIconv_INCLUDE_DIR="${ICONV_DIR}" \
      -DPLATFORM="${CMAKE_PLATFORM}" \
      -DBUILD_SHARED_LIBS=YES \
      -DENABLE_BITCODE=NO \
      -DMZ_BZIP2=OFF \
      -DMZ_COMPAT=OFF \
      -DMZ_LZMA=OFF \
      -DMZ_SIGNING=OFF \
      -DMZ_WZAES=OFF \
      -DMZ_ZSTD=OFF

    cmake --build . \
      --config "${TARGET}" \
      --target install \
      -- \
      CODE_SIGN_IDENTITY="" \
      CODE_SIGNING_REQUIRED=NO \
      CODE_SIGNING_ALLOWED=NO
  done

  SIM_LIB="${WORK_DIR}/out/simulator/${TARGET}/lib/libminizip.3.0.4.dylib"
  DEV_LIB="${WORK_DIR}/out/device/${TARGET}/lib/libminizip.3.0.4.dylib"

  OUT_DIR="${ROOT}/out/ios/${TARGET}"
  mkdir -p "${OUT_DIR}"

  rm -f "${OUT_DIR}/libminizip.3.0.4.dylib"
  lipo -create \
    "${SIM_LIB}" \
    "${DEV_LIB}" \
    -output "${OUT_DIR}/libminizip.3.dylib"

  cp -a "${WORK_DIR}/out/device/${TARGET}/include" "${OUT_DIR}"
done
