import UIKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        openZipFile()
    }
}

func openZipFile() {
    let zipFilePath = Bundle.main.path(forResource: "minizip-ng-master", ofType: "zip")!

    let zipReader = UnsafeMutablePointer<UnsafeMutableRawPointer?>.allocate(capacity: 1)
    mz_zip_reader_create(zipReader)
    mz_zip_reader_open_file(zipReader.pointee, zipFilePath.cString(using: .utf8))

    mz_zip_reader_goto_first_entry(zipReader.pointee)
    repeat {
        let info = UnsafeMutablePointer<UnsafeMutablePointer<mz_zip_file>?>.allocate(capacity: 1)
        mz_zip_reader_entry_get_info(zipReader.pointee, info)

        let filename = String(cString: info.pointee!.pointee.filename!)
        print(filename)
    } while mz_zip_reader_goto_next_entry(zipReader.pointee) == MZ_OK

    mz_zip_reader_close(zipReader.pointee)
    mz_zip_reader_delete(zipReader)
}
