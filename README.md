# build_minizip_mobile

Build [zlib-ng/minizip-ng](https://github.com/zlib-ng/minizip-ng) for iOS/Android with macOS.

Use [leetal/ios-cmake](https://github.com/leetal/ios-cmake) for iOS build.

## Notes

* Compatibility layer is disabled
  * `-DMZ_COMPAT=OFF`
* Zip signing and WinZIP AES encryption are disabled
  * `-DMZ_SIGNING=OFF -DMZ_WZAES=OFF`
* Zstd, LZMA and Bzip2 are disabled
  * `-DMZ_ZSTD=OFF -DMZ_LZMA=OFF -DMZ_BZIP2=OFF`
* iOS: Bitcode is disabled
  * `-DENABLE_BITCODE=NO`

## Requirements

* cmake, pkg-config (typically: `brew install cmake pkg-config`)
* iOS
  * Xcode with command line tools
* Android
  * Android SDK with NDK 21 (e.g. 21.4.7075529)
  * NDK 21+ (22, 23...) is not supported because of `typedef redefinition with different types ('unsigned long' vs 'unsigned int')` error on 32bit build
    * See also:
      * https://github.com/zlib-ng/minizip-ng/blob/3.0.4/mz_crypt.c#L37-L39
      * https://android.googlesource.com/toolchain/prebuilts/ndk/r23/+/refs/heads/master/toolchains/llvm/prebuilt/linux-x86_64/sysroot/usr/include/zlib.h#82

## Setup

Make sure submodules are available.

```bash
git submodule update --init --recursive
```

### Android

Set `ANDROID_SDK_ROOT` (optional) and `NDK` environment variables.

```bash
# example

export ANDROID_SDK_ROOT="${HOME}/Library/Android/sdk"
export NDK="${ANDROID_SDK_ROOT}/ndk/21.4.7075529"
```

## Build

```bash
# iOS
sh build-ios.sh

# Android
sh build-android.sh
```

## Output

```txt
out/
├── android
│   ├── Debug
│   │   └── jniLibs
│   │       ├── arm64-v8a
│   │       │   └── libminizip.so
│   │       ├── armeabi-v7a
│   │       │   └── libminizip.so
│   │       ├── x86
│   │       │   └── libminizip.so
│   │       └── x86_64
│   │           └── libminizip.so
│   └── Release
│       └── jniLibs
│           ├── arm64-v8a
│           │   └── libminizip.so
│           ├── armeabi-v7a
│           │   └── libminizip.so
│           ├── x86
│           │   └── libminizip.so
│           └── x86_64
│               └── libminizip.so
└── ios
    ├── Debug
    │   ├── include
    │   │   ├── mz.h
    │   │   ...
    │   │   └── mz_zip_rw.h
    │   └── libminizip.3.dylib
    └── Release
        ├── include
        │   ├── mz.h
        │   ...
        │   └── mz_zip_rw.h
        └── libminizip.3.dylib
```

## How to use on iOS

* Add debug or release `libminizip.3.dylib` to `Frameworks, Libraries, and Embedded Content`
* Add include directory to `Header Search Paths`
* Add parent directory of `libminizip.3.dylib` to `Library Search Paths`
* Create bridging header when using Swift

You can generate example xcodeproj with [yonaskolb/XcodeGen](https://github.com/yonaskolb/XcodeGen).

```bash
brew install xcodegen
cd examples/ExampleIOS/
xcodegen generate && open ExampleIOS.xcodeproj
```

## How to use on Android

* Copy debug or release `jniLibs/` to `src/main/`
